$(document).ready(function() {
    const checkbox = $("input[type=checkbox]");

    checkbox.change(function(event) {
        $('#filter-form').submit();
    });
});
