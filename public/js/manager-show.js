$(document).ready(function() {
    $('#accept').click(function(event) {
        const url = new URL(window.location.href);
        const token = url.searchParams.get("token");

        const type = 'POST';
        let href;

        if(token) {
            href = $('#accept-url').val() + '?token=' + token;
        } else {
            href = $('#accept-url').val();
        }

        const data = {};

        request(type, href, data, function() {
            location.reload();
        });
    });
});
