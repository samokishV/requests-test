$(document).ready(function() {
    $('a.disabled').on('click',function(event){
        event.preventDefault();
    });

    $('a#cancel:not(.disabled)').click(function(event) {
        event.preventDefault();
  
        const type = 'DELETE';
        const href = this.getAttribute('href');
        const data = {};
    
        request(type, href, data, function() {
            location.reload();
        });
    });
});
