<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth', 'auth.user'])->group(function() {
    Route::get('/requests', 'RequestController@index')->name('requests.index');
    Route::get('/requests/create', 'RequestController@create')->name('requests.create');
    Route::get('/requests/{id}', 'RequestController@show')->name('requests.show');
    Route::post('/requests', 'RequestController@store')->name('requests.store');
    Route::delete('/requests/{id}', 'RequestController@destroy')->name('requests.destroy');
}); 

Route::prefix('manager')->name('manager.')->middleware(['auth', 'auth.manager'])->group(function () {
    Route::get('requests', 'ManagerRequestController@index')->name('requests.index');
});

Route::prefix('manager')->name('manager.')->middleware(['auth.jwt'])->group(function () {
    Route::get('requests/{id}', 'ManagerRequestController@show')->name('requests.show');
    Route::post('requests/{id}/answer', 'ManagerRequestController@answer')->name('requests.answer');
    Route::post('requests/{id}/accept', 'ManagerRequestController@accept')->name('requests.accept');
});

Route::get('/storage/app/uploads/{fileName}', function($fileName) {
    $destinationPath = base_path() . "/storage/app/uploads/" . $fileName;
    return response()->download($destinationPath);
});
