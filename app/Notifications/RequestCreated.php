<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RequestCreated extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data, $uploads, $token)
    {
        $this->id = $data->id;
        $this->title = $data->title;
        $this->message = $data->message;
        $this->token = $token;

        if(isset($uploads)) {
            $this->uploads = [
                'path' => base_path().'/storage/app/'.$data->uploads,
                'originalName' => $uploads->getClientOriginalName(),
                'mimeType' => $uploads->getClientMimeType()
            ];
        }
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $view = (new MailMessage)
            ->view('emails.create', ['bodyMessage' => $this->message, 'token' => $this->token, 'id' => $this->id])
            ->subject($this->title);
        
        if(isset($this->uploads)) {
            return $view->attach($this->uploads['path'], [
                'as' => $this->uploads['originalName'],
                'mime' => $this->uploads['mimeType'],
            ]);
        } 

        return $view;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
