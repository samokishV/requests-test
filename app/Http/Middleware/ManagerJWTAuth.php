<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use JWTAuth;
use JWTFactory;

class ManagerJWTAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
            app('auth')->setUser($user);
            return $next($request);
        } catch(\Exception $e) {
            return $next($request);
        }
    }
}
