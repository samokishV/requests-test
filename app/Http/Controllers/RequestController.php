<?php

namespace App\Http\Controllers;

use App\User;
use JWTAuth;
use Illuminate\Http\Request;
use App\Request as UserRequest;
use Illuminate\Support\Facades\Auth;
use App\Notifications\RequestClosed;
use App\Notifications\RequestCreated;
use App\Http\Requests\StoreRequest as StoreRequest;

class RequestController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $userRequests = $user->requests()->orderBy('id', 'DESC')->get();

        return view('request.index', ['userRequests' => $userRequests]);
    }

    public function create()
    {
        return view('request.create');
    }

    public function store(StoreRequest $request)
    {
        $user = Auth::user();
        $userRequest = new UserRequest();
        $todayRequest = $userRequest->today($user->id)->first();

        if($todayRequest) {
            return back()->withInput()
                ->with('error_message', __('site.request.today.exist.error'));
        }

        if(null !== $request->file('uploads')) {
            $filePath = $request->file('uploads')->store('uploads');
            $userRequest->uploads = $filePath;
        }

        if($request->parent_id) {
            $parentRequest = (new UserRequest())->find($request->parent_id);
            $parentManagerId = $parentRequest->manager_id;
        }

        if(isset($parentManagerId)) {
            $userRequest->manager_id = $parentManagerId;
        }

        $userRequest->fill($request->all());
        $userRequest->user()->associate($user);
        $userRequest->save();

        if(!isset($parentManagerId)) {
            $managers = (new User())->where('role', 'manager')->get();

            foreach($managers as $manager) {
                $token = JWTAuth::fromUser($manager);
                $manager->notify(new RequestCreated($userRequest, $request->file('uploads') ?? null, $token));
            }
        } else {
            $manager = (new User())->find($parentManagerId);
            $token = JWTAuth::fromUser($manager);
            $manager->notify(new RequestCreated($userRequest, $request->file('uploads') ?? null, $token));
        }

        return redirect()->route('requests.index');
    }

    public function show($id)
    {
        $userRequest = (new UserRequest())->find($id);
        if(!$userRequest) {
            abort(404);
        }

        return view('request.show', ['userRequest' => $userRequest]);
    }

    public function destroy($id)
    {
        $user = Auth::user();

        $userRequest = (new UserRequest())->with('user')->find($id);
        $userRequest->status = 'closed';
        $userRequest->save();

        $manager = (new User())->find($userRequest->manager_id);

        if($manager) {
            $manager->notify(new RequestClosed($user, $userRequest));
        }
    }
}
