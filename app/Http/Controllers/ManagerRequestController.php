<?php

namespace App\Http\Controllers;

use App\User;
use JWTAuth;
use JWTFactory;
use Illuminate\Http\Request;
use App\Request as UserRequest;
use Illuminate\Support\Facades\Auth;
use App\Notifications\RequestAnswer;
use App\Http\Requests\StoreRequest as StoreRequest;

class ManagerRequestController extends Controller
{
    public function index()
    {
        $viewed = $_GET['viewed'] ?? [];
        $hasAnswer = $_GET['has_answer'] ?? [];
        $status = $_GET['status'] ?? [];
        $accepted = $_GET['accepted'] ?? [];

        $userRequests = (new UserRequest())->orderBy('id', 'DESC')->filter($viewed, $hasAnswer, $status, $accepted)->get();

        return view('manager.request.index', ['userRequests' => $userRequests]);
    }

    public function show($id)
    {
        if (null == Auth::user() || !Auth::user()->role == 'manager') {
            return redirect('login');
        }

        $userRequest = (new UserRequest())->find($id);
        if(!$userRequest) {
            abort(404);
        }

        $userRequest->viewed = 1;
        $userRequest->save();

        return view('manager.request.show', ['userRequest' => $userRequest]);
    }

    public function answer(Request $request, $id)
    {
        $userRequest = (new UserRequest())->find($id);
        if(!$userRequest) {
            abort(404);
        }

        $userRequest->has_answer = 1;
        $userRequest->save();

        $userRequest->user->notify(new RequestAnswer($request->message, $userRequest));

        return redirect()->route('manager.requests.index');
    }

    public function accept(Request $request, $id)
    {
        $userRequest = (new UserRequest())->find($id);
        if(!$userRequest) {
            abort(404);
        }

        $manager = Auth::user();
        $userRequest->manager_id = $manager->id;
        $userRequest->save();
    }
}
