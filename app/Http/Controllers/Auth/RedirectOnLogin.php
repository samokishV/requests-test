<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;

trait RedirectOnLogin 
{
    public function redirectTo()
    {
        if (Auth::user()->role == "user") {
            return route('requests.index');
        }

        if (Auth::user()->role == "manager") {
            return route('manager.requests.index');
        }

        return '/home';
    }
}
