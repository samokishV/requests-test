<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    protected $fillable = ['title', 'message', 'parent_id'];

    public function user() 
    {
        return $this->belongsTo('App\User');
    }

    public function scopeToday($query, $id)
    {
        return $query->whereDate('created_at', Carbon::today())
            ->where('user_id', $id);
    }

    public function scopeFilter($query, $viewed, $hasAnswer, $status, $accepted) 
    {
        if(count($viewed) == 1) {
            $viewed = self::stringToBool($viewed[0]);
            $query = $query->where('viewed', '=', $viewed);
        }

        if(count($hasAnswer) == 1) {
            $hasAnswer = self::stringToBool($hasAnswer[0]);
            $query = $query->where('has_answer', '=', $hasAnswer);
        }

        if(count($status) == 1) {
            $query = $query->where('status', '=', $status);
        }

        if(count($accepted) == 1) {
            $accepted = self::stringToBool($accepted[0]);

            if($accepted) {
                $query = $query->where('manager_id', '!=', NULL);
            } else {
                $query = $query->whereNull('manager_id');
            }
            
        }

        return $query;
    }

    public static function stringToBool($value)
    {
        return (int)filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }
}
