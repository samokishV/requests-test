<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('message');
            $table->string('uploads')->nullable();
            $table->enum('status', ['active', 'closed'])->default('active');
            $table->boolean('viewed')->default(0);
            $table->boolean('has_answer')->default(0);
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('manager_id')->unsigned()->nullable();
            $table->bigInteger('parent_id')->default(0);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('manager_id')->references('id')->on('users')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
