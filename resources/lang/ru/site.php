<?php

return [
    'index.header' => 'Новая заявка',
    'index.answer.btn' => 'Ответить',
    'index.cancel.btn' => 'Отклонить',
    'index.requests.empty' => 'Вы еще не отправили ни одной заявки.',
    'create.back' => 'К списку заявок',
    'create.header' => 'Заявка',
    'create.lable.title' => 'Тема',
    'create.lable.message' => 'Сообщение',
    'create.lable.attachment' => 'Добавить вложение',
    'create.submit.btn' => 'Отправить',
    'manager.requests.viewed' => 'Просмотренные',
    'manager.requests.not.viewed' => 'Не просмотренные',
    'manager.requests.has.answer' => 'Есть ответ',
    'manager.requests.has.no.answer' => 'Нет ответа',
    'manager.requests.active' => 'Открытые',
    'manager.requests.closed' => 'Закрытые',
    'manager.requests.accepted' => 'Есть исполнитель',
    'manager.requests.not.accepted' => 'Нет исполнителя',
    'manager.request.viewed' => 'просмотрено',
    'manager.request.has.answer' => 'есть ответ',
    'yes' => 'да',
    'manager.requests.empty' => 'Список заявок пуст.',
    'username' => 'Имя',
    'email' => 'Email',
    'uploads' => 'Вложения',
    'manager.request.accept.btn' => 'Принять заявку',
    'request.today.exist.error' => 'Вы можете отправить только одну заявку в сутки!'
];