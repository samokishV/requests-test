@extends('layouts.app')

@section('content')
    <table class="table table-striped" style="width: 600px">
        <p>
            <a href="{{ route('requests.create') }}"><button class="btn btn-sm btn-success">{{__('site.index.header')}}</button></a>
        </p>
        @forelse($userRequests as $request)
        <tr>
            <td>{{ $request->created_at }}</td>
            <td>
                <a href="{{ route('requests.show', ['id' => $request->id]) }}">{{ $request->title }}</a>
            </td>
            <td>
                <a href="{{ route('requests.create', ['id' => $request->id]) }}" class="@if($request->status == 'closed') disabled @endif">
                    <button class="btn btn-sm btn-primary @if($request->status == 'closed') disabled @endif">{{__('site.index.answer.btn')}}</button>
                </a>
            </td>
            <td>
                <a id="cancel" href="{{ route('requests.destroy', ['id' => $request->id]) }}" class="@if($request->status == 'closed') disabled @endif">
                    <button class="btn btn-sm btn-danger @if($request->status == 'closed') disabled @endif">{{__('site.index.cancel.btn')}}</button>
                </a>
            </td>
        </tr>
        @empty
            <p>{{__('site.index.requests.empty')}}</p>
        @endforelse
    </table>
@endsection

<script src="{{ asset('js/ajax.js') }}" defer></script>
<script src="{{ asset('js/index.js') }}" defer></script>