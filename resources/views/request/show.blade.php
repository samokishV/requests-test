@extends('layouts.app')

@section('content')
    <a href="{{ route('requests.index') }}">{{__('site.create.back')}}</a>

    <table class="table table-striped" style="width: 1000px">
        <tr><td>{{ __('site.create.lable.title') }}:</td><td>{{ $userRequest->title }}</td></tr>
        <tr><td>{{ __('site.create.lable.message') }}:</td><td>{{ $userRequest->message }}</td></tr>
        <tr><td>{{ __('site.uploads') }}:</td><td><a href="{{ asset('storage/app/' . $userRequest->uploads) }}" download>{{ $userRequest->uploads }}</a></td></tr>
    </table>
@endsection   
