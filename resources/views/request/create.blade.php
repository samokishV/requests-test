@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col px-md-5">
            @if (Session::get('error_message'))
                <div class="alert alert-danger">
                    {{ Session::get('error_message') }}
                </div>
            @endif
            <form action="{{ route('requests.store') }}" method="post" id="user-add" enctype="multipart/form-data">
                @csrf
                <a href="{{ route('requests.index') }}">{{ __('site.create.back') }}</a>
                <input type="hidden" value="{{ $_GET['id'] ?? 0 }}" name="parent_id" id="parent_id">
                <p><h3>{{ __('site.create.header') }}</h3></p>
                <div class="form-group">
                    <label for="title">{{ __('site.create.lable.title') }}</label>
                    <input type="title" name="title" value="{{old('title')}}" class="form-control" required id="title">
                    @if ($errors->has('title'))
                        <div class="alert alert-danger">
                            {{ $errors->first('title') }}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="message">{{ __('site.create.lable.message') }}</label>
                    <textarea type="message" name="message" class="form-control" required id="message">{{ old('message') }}</textarea>
                    @if ($errors->has('message'))
                        <div class="alert alert-danger">
                            {{ $errors->first('message') }}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="uploads">{{ __('site.create.lable.attachment') }}</label>
                    <input type="file" class="form-control-file" id="uploads" name="uploads">
                    @if ($errors->has('uploads'))
                        <div class="alert alert-danger">
                            {{ $errors->first('uploads') }}
                        </div>
                    @endif
                </div>
                <input type="submit" class="btn btn-success btn-lg w-100" value="{{ __('site.create.submit.btn') }}">
            </form>
        </div>
    </div>
@endsection