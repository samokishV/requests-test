<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>

<table>
    <tr>
        <td>{{ $bodyMessage }}</td>
        <td><a href="{{ route('manager.requests.show', $id).'?token='.$token }}">{{ route('manager.requests.show', $id).'?token='.$token }}</a></td>
    </tr>
</table>
</body>
</html>