@extends('layouts.app')

@section('content')
    <a href="{{ route('manager.requests.index') }}">{{__('site.create.back')}}</a>

    <table class="table table-striped" style="width: 1000px">
        <tr><td>{{ __('site.username') }}:</td><td>{{ $userRequest->user->username }}</td></tr>
        <tr><td>{{ __('site.email') }}:</td><td>{{ $userRequest->user->email }}</td></tr>
        <tr><td>{{ __('site.create.lable.title') }}:</td><td>{{ $userRequest->title }}</td></tr>
        <tr><td>{{ __('site.create.lable.message') }}:</td><td>{{ $userRequest->message }}</td></tr>
        <tr><td>{{ __('site.uploads') }}:</td><td><a href="{{ asset('storage/app/' . $userRequest->uploads) }}" download>{{ $userRequest->uploads }}</a></td></tr>
    </table>

    @if(!isset($userRequest->manager_id)) 
        <input type="hidden" id="accept-url" value="{{ route('manager.requests.accept', ['id' => $userRequest->id]) }}">
        <button type="button" id="accept" class="btn btn-success">{{__('site.manager.request.accept.btn')}}</button>
    @endif

    @if(isset($userRequest->manager_id) && $userRequest->manager_id == Auth::user()->id)
        <form action="{{ route('manager.requests.answer', ['id' => $userRequest->id]) }}" method="post" id="requests-answer" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="message">{{__('site.create.lable.message')}}</label>
                <textarea type="message" name="message" class="form-control" required id="message">{{ old('message') }}</textarea>
                @if ($errors->has('message'))
                    <div class="alert alert-danger">
                        {{ $errors->first('message') }}
                    </div>
                @endif
            </div>
            <input type="submit" class="btn btn-success btn-lg w-100" value="{{__('site.create.submit.btn')}}">
        </form>
    @endif    
@endsection

<script src="{{ asset('js/ajax.js') }}" defer></script>
<script src="{{ asset('js/manager-show.js') }}" defer></script>