@extends('layouts.app')

@section('content')
    <form id="filter-form">
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="viewed" name="viewed[]" value="true"
            @if(isset($_GET['viewed']) && in_array("true", $_GET['viewed'])) ? checked @endif>
            <label class="form-check-label" for="viewed">{{__('site.manager.requests.viewed')}}</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="not-viewed" name="viewed[]" value="false"
            @if(isset($_GET['viewed']) && in_array("false", $_GET['viewed'])) ? checked @endif>
            <label class="form-check-label" for="not-viewed">{{__('site.manager.requests.not.viewed')}}</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="has-answer" name="has_answer[]" value="true"
            @if(isset($_GET['has_answer']) && in_array("true", $_GET['has_answer'])) ? checked @endif>
            <label class="form-check-label" for="has-answer">{{__('site.manager.requests.has.answer')}}</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="has-no-answer" name="has_answer[]" value="false"
            @if(isset($_GET['has_answer']) && in_array("false", $_GET['has_answer'])) ? checked @endif>
            <label class="form-check-label" for="has-no-answer">{{__('site.manager.requests.has.no.answer')}}</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="open" name="status[]" value="active"
            @if(isset($_GET['status']) && in_array("active", $_GET['status'])) ? checked @endif>
            <label class="form-check-label" for="open">{{__('site.manager.requests.active')}}</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="closed" name="status[]" value="closed"
            @if(isset($_GET['status']) && in_array("closed", $_GET['status'])) ? checked @endif>
            <label class="form-check-label" for="closed">{{__('site.manager.requests.closed')}}</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="accepted" name="accepted[]" value="true"
            @if(isset($_GET['accepted']) && in_array("true", $_GET['accepted'])) ? checked @endif>
            <label class="form-check-label" for="closed">{{__('site.manager.requests.accepted')}}</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="accepted" name="accepted[]" value="false"
            @if(isset($_GET['accepted']) && in_array("false", $_GET['accepted'])) ? checked @endif>
            <label class="form-check-label" for="closed">{{__('site.manager.requests.not.accepted')}}</label>
        </div>
    </form>

    <table class="table table-striped" style="width: 1000px">
        <thead class="thead-dark">
            <tr>
                <th>created_at</th>
                <th>user_name</th>
                <th>title</th>
                <th>status</th>
                <th>viewed</th>
                <th>has_answer</th>
                <th>assign_to_me</th>
            </tr>
        <thead>
        <tbody>
        @forelse($userRequests as $request)
        <tr>
            <td>{{ $request->created_at }}</td>
            <td>{{ $request->user->name }}</td>
            <td><a href="{{ route('manager.requests.show', ['id' => $request->id]) }}">{{ $request->title }}</a></td>
            <td>{{ $request->status }}</td>
            <td>{{ $request->viewed ? __('site.manager.request.viewed') : '' }}</td>
            <td>{{ $request->has_answer ? __('site.manager.request.has.answer') : '' }}</td>
            <td>{{ ($request->manager_id == Auth::user()->id) ? __('site.yes') : '' }}</td>
        </tr>
        @empty
            <p>{{__('site.manager.requests.empty')}}</p>
        @endforelse
        </tbody>
    </table>
@endsection

<script src="{{ asset('js/manager-index.js') }}" defer></script>