
## Request manager
For collecting user requests and answering for them.

## Installation

```
cp .env.example .env
Change db and mail config values in .env file
composer install
php artisan migrate --seed
php artisan key:generate
php artisan config:clear
php artisan jwt:secret
php artisan storage:link
php artisan serve
```

## Manager access
```
login main.manager@gmail.com
password password
```
